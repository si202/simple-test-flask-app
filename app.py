import time
import datetime
import redis
import socket
from flask import Flask, render_template, jsonify
from flask_restplus import Api

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

def get_container_id():
	# type: () -> object
	return socket.gethostname()

@app.route('/')
def hello():
    count = get_hit_count()
    container = get_container_id()
    return 'Hello World! I have been seen %s times.\n This container is %s\n' % (format(count), container)

@app.route('/container')
def index():
    container = get_container_id()
    return render_template('container.html', title='Container', container=container)

@app.route('/api/v1.0/hits')
def hits():
    data = {"datetime": datetime.datetime.now().isoformat(),
            "hits": get_hit_count()}
    return jsonify(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
